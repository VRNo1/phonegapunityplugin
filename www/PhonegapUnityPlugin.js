cordova.define("cordova/plugin/unity3d", function(require, exports, module)
{
	PhonegapUnityPlugin = {};

	PhonegapUnityPlugin.initialize = function()
    {
        console.log("Initializing Phonegap Unity3D plugin");
    };

    PhonegapUnityPlugin.display = function( )
    {
        var options = [];

        cordova.exec( function(param) { console.log("Success"); }, function(param) { console.log("Failure");}, 'PhonegapUnityPlugin', 'display', []);
    };

    module.exports = PhonegapUnityPlugin;
});