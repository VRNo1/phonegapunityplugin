package com.sojournermobile.unity;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;


/**
 * @author gpierce
 */

public class PhonegapUnityPlugin extends CordovaPlugin 
{
	public static final String TAG = "PhonegapUnityPlugin";
	
	public static final String DISPLAY = "display";

	private static CordovaWebView gWebView;

	private static Bundle gCachedExtras = null;
    private static boolean gForeground = false;

	/**
	 * Gets the application context from cordova's main activity.
	 * @return the application context
	 */
	private Context getApplicationContext() 
	{
		return this.cordova.getActivity().getApplicationContext();
	}

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        gForeground = true;
    }
	
	@Override
    public void onPause(boolean multitasking) {
        super.onPause(multitasking);
        gForeground = false;
    }

    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);
        gForeground = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        gForeground = false;
		gWebView = null;
    }


	@Override
	public boolean execute(String action, JSONArray data, CallbackContext callbackContext) 
	{
		boolean result = false;

		Log.v(TAG, "execute: action=" + action);

		if (DISPLAY.equals(action)) 
		{

			Log.v(TAG, "execute: data=" + data.toString());

			try 
			{

				//JSONObject jo = data.getJSONObject(0);
				
				gWebView = this.webView;
				//Log.v(TAG, "execute: jo=" + jo.toString());

				result = true;
				callbackContext.success();
			} 
			catch (Exception e) 
			{
				Log.e(TAG, "execute: Got JSON Exception " + e.getMessage());
				result = false;
				callbackContext.error(e.getMessage());
			}

		} 
		else 
		{
			result = false;
			Log.e(TAG, "Invalid action : " + action);
			callbackContext.error("Invalid action : " + action);
		}

		return result;
	}


    public static boolean isInForeground()
    {
      return gForeground;
    }

    public static boolean isActive()
    {
    	return gWebView != null;
    }
}
