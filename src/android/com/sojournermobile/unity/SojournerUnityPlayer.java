package com.sojournermobile.unity;

import com.unity3d.player.UnityPlayerActivity;

import android.os.Bundle;
import android.util.Log;

public class SojournerUnityPlayer extends UnityPlayerActivity 
{
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate( savedInstanceState );

		Log.d("SojournerUnityPlayer", "onCreate() called!");
	}
}